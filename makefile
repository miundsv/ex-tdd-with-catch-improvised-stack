P=test-main
OBJECTS=test-stack.o test-main.o
CXXFLAGS=-g -Wall -Werror -O3
LDLIBS=
CXX=g++ -std=c++17
CC=$(CXX)

$(P): $(OBJECTS)

test: $(P)
	./test-main

clean:
	-@rm *.o $(P)
