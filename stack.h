#ifndef _STACK_H_
#define _STACK_H_

#include <stdexcept>
#include <algorithm>

template<typename T>
class Stack {
  private:
    T* elems;
    int top{0};
    int capacity{1};
  public:
    Stack();
    ~Stack();
    bool empty() const noexcept;
    unsigned int size() const noexcept;
    T& pop();
    void push(const T& e) noexcept;
};

template<typename T>
Stack<T>::Stack() {
  elems = new T[capacity];
}

template<typename T>
Stack<T>::~Stack() {
  delete[] elems;
}

template<typename T>
bool Stack<T>::empty() const noexcept {
  return size() == 0;
}

template<typename T>
unsigned int Stack<T>::size() const noexcept {
  return top;
}

template<typename T>
T& Stack<T>::pop() {
  if (empty()) {
    throw std::range_error{"Can't pop empty stack"};
  }
  return elems[--top];
}

template<typename T>
void Stack<T>::push(const T& e) noexcept {
  // First grow if necessary.
  if (top >= capacity) {
    auto old_elems = elems;
    auto new_cap = capacity*2;
    auto new_elems = new T[new_cap];
    std::copy(old_elems, old_elems+capacity, new_elems);
    capacity = new_cap;
    elems = new_elems;
    delete[] old_elems;
  }
  elems[top++] = e;
}

#endif
